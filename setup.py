# encoding=utf-8
from setuptools import setup, find_namespace_packages

setup(
    name='aiotumblr-public',
    version='0.2.1',
    description='Public API extension for AIOTumblr',
    author='Lena',
    author_email='arlena@hubsec.eu',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'Programming Language :: Python :: 3.7'
    ],
    packages=find_namespace_packages(),
    install_requires=['aiotumblr-core>=0.1.3.1'],  # FIXME: 0.2 update this to >=0.2
)
